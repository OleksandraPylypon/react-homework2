import React, { Component } from "react";
import ProductsList from "./components/products/ProductsList";
import Header from "./components/header";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartCount: 0,
      favoritesCount: 0,
    };
  }

  handleChangeCartCount = (number) => {
    this.setState((prevState) => ({
      cartCount: prevState.cartCount + number,
    }));
  };

  handleChangeFavoritesCount = (number) => {
    this.setState((prevState) => ({
      favoritesCount: prevState.favoritesCount + number,
    }));
  };

  render() {
    const { cartCount, favoritesCount } = this.state;

    return (
      <div className="App">
        <Header favoritesCount={favoritesCount} cartCount={cartCount} />
        <ProductsList
          handleChangeFavoritesCount={this.handleChangeFavoritesCount}
          handleChangeCartCount={this.handleChangeCartCount}
        />
      </div>
    );
  }
}

export default App;
