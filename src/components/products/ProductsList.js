import React, { Component } from "react";
import ProductsCard from "./productsCard";
import "./Products.scss";

class ProductsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    fetch("./products.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState({ products: data });
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }

  render() {
    const { handleChangeCartCount, handleChangeFavoritesCount } = this.props;
    const { products } = this.state;

    return (
      <div className="products-list">
        {products.map((product, index) => (
          <ProductsCard
            key={index}
            imagePath={product.imagePath}
            name={product.name}
            articleNumber={product.articleNumber}
            color={product.color}
            price={product.price}
            handleChangeFavoritesCount={handleChangeFavoritesCount}
            handleChangeCartCount={handleChangeCartCount}
          />
        ))}
      </div>
    );
  }
}

export default ProductsList;
