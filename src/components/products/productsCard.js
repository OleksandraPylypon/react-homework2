import React, { Component } from "react";
import Modal from "../Modal";
import "./Products.scss";

class ProductsCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      isAutoCloseEnabled: false,
      isFavorite: false,
      isAddedToCart: false,
    };
  }

  openModal = () => {
    this.setState({
      isModalOpen: true,
      isAutoCloseEnabled: true,
    });

    this.props.handleChangeCartCount(this.state.isAddedToCart ? -1 : 1);

    this.setState((prevState) => ({
      isAddedToCart: !prevState.isAddedToCart,
    }));

    setTimeout(() => {
      this.setState({
        isModalOpen: false,
        isAutoCloseEnabled: false,
      });
    }, 1000);
  };

  toggleFavorite = () => {
    this.props.handleChangeFavoritesCount(this.state.isFavorite ? -1 : 1);

    this.setState((prevState) => ({
      isFavorite: !prevState.isFavorite,
    }));
  };

  render() {
    return (
      <div className="products-card">
        <img
          className={`icon-star ${this.state.isFavorite ? "favorite" : ""}`}
          src={
            this.state.isFavorite
              ? "/images/star-yellow.svg"
              : "/images/starblack.svg"
          }
          alt="star-icon"
          onClick={this.toggleFavorite}
        />
        <img className="products-image" src={this.props.imagePath} alt="" />
        <p className="products-articleNumber">
          Артикул: {this.props.articleNumber}
        </p>
        <p className="products-name">{this.props.name}</p>
        <p className="products-price">{this.props.price}</p>
        <p className="products-color">Color: {this.props.color}</p>
        <button className="products-button" onClick={this.openModal}>
          Add to cart
        </button>

        {this.state.isModalOpen && (
          <Modal text={`You have added ${this.props.name} to your cart.`} />
        )}
      </div>
    );
  }
}

export default ProductsCard;
