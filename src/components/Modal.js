import React, { Component } from "react";
import styles from "./Modal.module.scss";

class Modal extends Component {
  render() {
    const { text } = this.props;

    return (
      <div className={styles["modal-overlay"]}>
        <div className={styles.modal}>
          <div className={styles["modal-content"]}>{text}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
