import React, { Component } from "react";
import "./header.scss";

class Header extends Component {
  render() {
    const { cartCount, favoritesCount } = this.props;

    return (
      <header className="header">
        <img className="star-icon" src="/images/star.svg" alt="Star Icon" />
        <div className="star-count">{favoritesCount}</div>
        <img className="cart-icon" src="/images/cart.svg" alt="Cart Icon" />
        <div className="cart-count">{cartCount}</div>
      </header>
    );
  }
}

export default Header;
